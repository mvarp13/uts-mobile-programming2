

## Aplikasi Wallet

Aplikasi wallet adalah aplikasi yang berguna untuk transaksi transaksi seperti, hotel, minuman & makanan dan tiket kereta untuk travelling kita

## Anggota Kelompok

* [M. Venerdi Azani Raynes Putra (18111213)](https://gitlab.com/mvarp13)

## Job Description Anggota

* [M. Venerdi Azani Raynes Putra](https://gitlab.com/mvarp13) : Membuat Aplikasi.

## Documentation

* Tampilan Menu
    <br><img src="assets/ss/menu.jpg"  width="340" height="640">

* Tampilan All
    <br><img src="assets/ss/all.jpg"  width="340" height="640">

* Tampilan Overview
    <br><img src="assets/ss/overview.jpg"  width="340" height="640">

* Tampilan Profile
    <br><img src="assets/ss/profile.jpg"  width="340" height="640">

 
## Terima Kasih
