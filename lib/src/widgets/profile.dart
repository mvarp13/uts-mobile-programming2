import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  final String nama;
  ProfileScreen({Key key, @required this.nama}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String namee;

  @override
  void initState() {
    super.initState();
    namee = widget.nama;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("Wallet"),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text("Home"),
                  value: 1,
                )
              ];
            },
            onSelected: (value) {
              if (value == 1) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MainScreen(nama: namee)));
              }
            },
          )
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        children: <Widget>[
          SizedBox(
            height: 30.0,
          ),
          Text(
            "Biodata",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 20.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30.0,
          ),
          Image.asset(
            "assets/images/logo.webp",
            height: 200.0,
          ),
          SizedBox(
            height: 30.0,
          ),
          Text(
            "Nama",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          Text(
            widget.nama,
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15.0,
          ),
          Text(
            "Hobby",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          Text(
            (widget.nama == "raynes")
                ? "Watching"
                : ((widget.nama == "Vallerian")
                    ? "Hindu"
                    : ((widget.nama == "Mentari")
                        ? "Islam"
                        : ((widget.nama == "Arin")
                            ? "Islam"
                            : ((widget.nama == "Wendy")
                                ? "Islam"
                                : "Tidak Tau")))),
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15.0,
          ),
          Text(
            "T/T/L",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          Text(
            (widget.nama == "raynes")
                ? "Bengkulu, 13 Maret 1998"
                : ((widget.nama == "Vallerian")
                    ? "Bali, 02 Mei 1998"
                    : ((widget.nama == "Mentari")
                        ? "Sumbawa, 06 Mei 1998"
                        : ((widget.nama == "Arin")
                            ? "Banyuwangi, 15 September 1997)"
                            : ((widget.nama == "Wendy")
                                ? "Mojokerto, 30 September 1997"
                                : "Tidak Tau")))),
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15.0,
          ),
          Text(
            "Alamat",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold),
          ),
          Text(
            (widget.nama == "raynes")
                ? "Jalan Cijagra"
                : ((widget.nama == "Vallerian")
                    ? "Tegal Gondo"
                    : ((widget.nama == "Mentari")
                        ? "Tegal Gondo"
                        : ((widget.nama == "Arin")
                            ? "Sumber Sekar"
                            : ((widget.nama == "Wendy")
                                ? "Sengkaling"
                                : "Tidak Tau")))),
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15.0,
          )
        ],
      ),
    );
  }

  MainScreen({String nama}) {}
}
